package com.appdirect.repository;

import com.appdirect.AddressBookConfig;
import com.appdirect.model.Person;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by shantanu on 21/12/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {AddressBookConfig.class})
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void shouldSavePerson() {

        Person person = newPerson();
        personRepository.save(person);

        Assert.assertNotNull(person.getId());
        Person savedPerson = personRepository.findOne(person.getId());
        Assert.assertNotNull(savedPerson);
        Assert.assertEquals(person.getName(), savedPerson.getName());
        Assert.assertEquals(person.getStreet(), savedPerson.getStreet());
        Assert.assertEquals(person.getCity(), savedPerson.getCity());
        Assert.assertEquals(person.getState(), savedPerson.getState());
        Assert.assertEquals(person.getZip(), savedPerson.getZip());
        Assert.assertEquals(person.getCountry(), savedPerson.getCountry());

    }

    @Test
    public void shouldUpdatePerson() {
        Person person = newPerson();

        personRepository.save(person);

        Person savedPerson = personRepository.findOne(person.getId());

        savedPerson.setName("Sherlock");
        personRepository.save(savedPerson);

        Person updatedPerson = personRepository.findOne(person.getId());
        Assert.assertEquals("Sherlock", updatedPerson.getName());
        Assert.assertEquals(person.getStreet(), updatedPerson.getStreet());
        Assert.assertEquals(person.getCity(), updatedPerson.getCity());
        Assert.assertEquals(person.getState(), updatedPerson.getState());
        Assert.assertEquals(person.getZip(), updatedPerson.getZip());
        Assert.assertEquals(person.getCountry(), updatedPerson.getCountry());

    }

    @Test
    public void shouldDeletePerson() {
        Person person = newPerson();

        personRepository.save(person);

        Assert.assertNotNull(person.getId());
        Person savedPerson = personRepository.findOne(person.getId());

        personRepository.delete(person.getId());
        Person deletedPerson = personRepository.findOne(person.getId());
        Assert.assertNull(deletedPerson);

    }

    private Person newPerson(){
        Person person = new Person();
        person.setName("Sherlock Holmes");
        person.setStreet("221, Bakers street");
        person.setCity("London");
        person.setState("UK");
        person.setZip("NW1 6XE");
        person.setCountry("England");

        return person;
    }
}
