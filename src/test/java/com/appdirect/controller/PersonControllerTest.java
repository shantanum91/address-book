package com.appdirect.controller;

import com.appdirect.model.Person;
import com.appdirect.service.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by shantanu on 21/12/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(PersonController.class)
public class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonService personService;

    @Test
    public void testGetPerson() throws Exception {
        Person person = new Person();
        person.setName("Sherlock Holmes");
        person.setStreet("221, Bakers street");
        person.setCity("London");
        person.setState("UK");
        person.setZip("NW1 6XE");
        person.setCountry("England");

        when(personService.getPerson(1L)).thenReturn(person);

        mockMvc.perform(get("/persons/1")).andExpect(status().isOk()).andExpect(jsonPath("$.name", is(person.getName())));
        verify(personService, times(1)).getPerson(1L);

    }
}
