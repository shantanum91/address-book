package com.appdirect;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by shantanu on 21/12/16.
 */

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages={"com.appdirect.repository"})
@EntityScan(basePackages={"com.appdirect.model"})
public class AddressBookConfig {
}
