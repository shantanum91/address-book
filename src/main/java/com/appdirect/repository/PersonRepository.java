package com.appdirect.repository;

import com.appdirect.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by shantanu on 20/12/16.
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

}
