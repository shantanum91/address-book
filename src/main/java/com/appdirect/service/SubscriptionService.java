package com.appdirect.service;

import com.appdirect.dto.SubscriptionResult;
import org.springframework.stereotype.Service;

/**
 * Created by shantanu on 20/12/16.
 */
@Service
public interface SubscriptionService {

    SubscriptionResult createSubscription(String eventUrl);
    SubscriptionResult cancelSubscription(String eventUrl);

}
