package com.appdirect.service;

import com.appdirect.model.Person;
import com.appdirect.repository.PersonRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by shantanu on 20/12/16.
 */

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonRepository personRepository;


    @Override
    public void createPerson(Person person) {
        personRepository.save(person);
    }

    @Override
    public void updatePerson(Long id, Person person) {
        Person orgPerson = getPerson(id);
        person.setId(orgPerson.getId());
        BeanUtils.copyProperties(person, orgPerson);
        personRepository.save(orgPerson);
    }

    @Override
    public Person getPerson(Long id) {
        return personRepository.findOne(id);
    }

    @Override
    public List<Person> findAllPersons() {
        return personRepository.findAll();
    }

    @Override
    public void deletePerson(Long id) {
        personRepository.delete(id);
    }
}
