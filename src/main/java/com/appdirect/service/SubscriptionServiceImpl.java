package com.appdirect.service;

import com.appdirect.dto.Event;
import com.appdirect.dto.SubscriptionResult;
import com.appdirect.exception.EventErrorCodes;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by shantanu on 20/12/16.
 */
@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    @Value("${consumer.key}")
    private String consumerKey;
    @Value("${consumer.secret}")
    private String consumerSecret;

    @Override
    public SubscriptionResult createSubscription(String eventUrl) {
        HttpURLConnection connection = null;
        try {
            connection = getSignedConection(eventUrl);
            Event event = parseXmlFromUrl(connection.getInputStream());
            System.out.println("Event Type: " + event.getType());
            System.out.println("Creator Name: " + event.getCreator().getFirstName() + " " + event.getCreator().getLastName());
            System.out.println("Creator Email " + event.getCreator().getEmail());

        } catch (JAXBException | MalformedURLException e) {
            e.printStackTrace();
            return new SubscriptionResult(false, "Error while creating subscription!", EventErrorCodes.INVALID_DATA.name());
        } catch (IOException e) {
            e.printStackTrace();
            try {
                if (connection != null && connection.getResponseCode() == 401)
                    return new SubscriptionResult(false, "Error while creating subscription!", EventErrorCodes.UNAUTHORIZED.name());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return new SubscriptionResult(false, "Error while creating subscription!", EventErrorCodes.UNKNOWN_ERROR.name());
        } catch (OAuthExpectationFailedException | OAuthMessageSignerException | OAuthCommunicationException e) {
            e.printStackTrace();
            return new SubscriptionResult(false, "Error while creating subscription!", EventErrorCodes.OAUTH_ERROR.name());
        }
        return new SubscriptionResult(true, "Subscription created successfully!", null);
    }

    @Override
    public SubscriptionResult cancelSubscription(String eventUrl) {
        HttpURLConnection connection = null;
        try {
            connection = getSignedConection(eventUrl);
            Event event = parseXmlFromUrl(connection.getInputStream());
            System.out.println("Event Type: " + event.getType());
            System.out.println("Creator Name: " + event.getCreator().getFirstName() + " " + event.getCreator().getLastName());
            System.out.println("Creator Email " + event.getCreator().getEmail());

        } catch (JAXBException | MalformedURLException e) {
            e.printStackTrace();
            return new SubscriptionResult(false, "Error while cancelling subscription!", EventErrorCodes.INVALID_DATA.name());
        } catch (IOException e) {
            e.printStackTrace();
            try {
                if (connection != null && connection.getResponseCode() == 401)
                    return new SubscriptionResult(false, "Error while cancelling subscription!", EventErrorCodes.UNAUTHORIZED.name());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return new SubscriptionResult(false, "Error while cancelling subscription!", EventErrorCodes.UNKNOWN_ERROR.name());
        } catch (OAuthExpectationFailedException | OAuthMessageSignerException | OAuthCommunicationException e) {
            e.printStackTrace();
            return new SubscriptionResult(false, "Error while cancelling subscription!", EventErrorCodes.UNAUTHORIZED.name());
        }
        return new SubscriptionResult(true, "Subscription cancelled!", null);
    }

    private Event parseXmlFromUrl(InputStream resourceStream) throws JAXBException, MalformedURLException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Event.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (Event) jaxbUnmarshaller.unmarshal(resourceStream);
    }

    private HttpURLConnection getSignedConection(String eventUrl) throws IOException, OAuthCommunicationException,
            OAuthExpectationFailedException, OAuthMessageSignerException {

        HttpURLConnection connection;
        URL url = new URL(eventUrl);
        OAuthConsumer oAuthConsumer = new DefaultOAuthConsumer(consumerKey, consumerSecret);
        connection = (HttpURLConnection) url.openConnection();
        oAuthConsumer.sign(connection);
        return connection;
    }

}
