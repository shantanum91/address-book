package com.appdirect.service;

import com.appdirect.model.Person;

import java.util.List;

/**
 * Created by shantanu on 20/12/16.
 */
public interface PersonService {
    void createPerson(Person person);
    void updatePerson(Long id,Person person);
    Person getPerson(Long id);
    List<Person> findAllPersons();
    void deletePerson(Long id);
}
