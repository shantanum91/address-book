package com.appdirect.exception;

/**
 * Created by shantanu on 29/12/16.
 */
public enum EventErrorCodes {
    UNAUTHORIZED,
    INVALID_DATA,
    OAUTH_ERROR,
    UNKNOWN_ERROR;
}
