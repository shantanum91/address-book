package com.appdirect.controller;

import com.appdirect.dto.SubscriptionResult;
import com.appdirect.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by shantanu on 20/12/16.
 */
@RestController
@RequestMapping("/subscription")
public class SubscriptionController {

    @Autowired
    SubscriptionService subscriptionService;

    @RequestMapping("/create")
    public SubscriptionResult create(@RequestParam("url") String url) {
        System.out.println("-----------------------URL-----------------------\n" + url);

        return subscriptionService.createSubscription(url);
    }

    @RequestMapping("/cancel")
    public SubscriptionResult cancel(@RequestParam("url") String url) {
        System.out.println("-----------------------URL-----------------------\n" + url);
        return subscriptionService.cancelSubscription(url);
    }
}
