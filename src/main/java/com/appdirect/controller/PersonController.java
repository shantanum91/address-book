package com.appdirect.controller;

import com.appdirect.model.Person;
import com.appdirect.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by shantanu on 20/12/16.
 */

@RestController
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/persons", method = RequestMethod.POST)
    public void createPerson(@RequestBody Person person) {
        personService.createPerson(person);
    }

    @RequestMapping(value = "/persons/{id}", method = RequestMethod.PUT)
    public void UpdatePerson(@PathVariable("id") Long id, @RequestBody Person person) {
        personService.updatePerson(id, person);
    }

    @RequestMapping(value = "/persons/{id}", method = RequestMethod.GET)
    public Person getPerson(@PathVariable("id") Long id) {
        return personService.getPerson(id);
    }

    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public List<Person> listAllPersons() {
        return personService.findAllPersons();
    }

    @RequestMapping(value = "/persons/{id}", method = RequestMethod.DELETE)
    public void deletePerson(@PathVariable("id") Long id) {
        personService.deletePerson(id);
    }
}
