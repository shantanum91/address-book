package com.appdirect.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by shantanu on 20/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Item {
    @XmlElement
    private String quantity;
    @XmlElement
    private String unit;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
