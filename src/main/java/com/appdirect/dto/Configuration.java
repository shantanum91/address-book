package com.appdirect.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by shantanu on 20/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Configuration {
    @XmlElement
    private Entry entry;

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }
}
